<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('garages', function (Blueprint $table) {
            $table->id();
            $table->text('marca')->nullable();
            $table->text('modello')->nullable();
            $table->bigInteger('anno')->nullable();
            $table->bigInteger('km')->nullable();
            $table->decimal('prezzo',10,2)->nullable();
            $table->text('colore')->nullable();
            $table->bigInteger('cilindrata')->nullable();
            $table->text('alimentazione')->nullable();
            $table->text('cambio')->nullable();
            $table->text('cavalli')->nullable();
            $table->text('info_avanzate')->nullable();
            $table->text('condizione')->nullable();
            $table->boolean('vetrina')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('garages');
    }
};
