<?php

use App\Http\Controllers\CarController;
use App\Http\Controllers\GarageController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
});
Route::get('/',[HomeController::class,'index'])->name('h');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/auto', [CarController::class, 'index'])->name('auto');
    Route::post('/storeCar', [GarageController::class, 'store'])->name('storeCar');
    
    Route::post('/imagesLoadCar', [GarageController::class, 'imagesLoadCar'])->name('imagesLoadCar');
    Route::post('/imagesLoadCar/delete', [GarageController::class, 'deleteImage'])->name('deleteImage');
    Route::post('/editCar{id}', [GarageController::class, 'edit'])->name('editCar');
    Route::post('/modifiedCar{garage}', [GarageController::class, 'modifiedCar'])->name('modifiedCar');
    Route::post('/deleteIMG', [GarageController::class, 'deleteIMG'])->name('deleteIMG');
});
Route::post('/detailCar{id}',[GarageController::class,'detailCar'])->name('detailCar');

Route::get('/marchiAll',[CarController::class,'MarchiAll'])->name('MarchiAll');
Route::post('/modelliAutoSel',[CarController::class,'modelliAutoSel'])->name('modelliAutoSel');
Route::post('/ricercaResult',[CarController::class,'ricercaResult'])->name('ricercaResult');
Route::post('/getTotaleMarchi',[CarController::class,'getTotaleMarchi'])->name('getTotaleMarchi');

require __DIR__.'/auth.php';
