
import './bootstrap';
import './funzioni';
import './loadImages';
import './drop';
import './owl.carousel';
/* Dropzone.autoDiscover = false; */


import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();
