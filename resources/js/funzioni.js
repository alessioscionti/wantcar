$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function () {
    let reparto=$('#reparto').val();
    //console.log(reparto);
    if (reparto==1) {
        $.ajax({
            type: "get",
            url: "marchiAll",
            data: {reparto:reparto},
            success: function (response) {
                //console.log(response);
                var options = []; // Array per le opzioni
                    options.push('<option value="" selected disabled>Seleziona Marca</option>')
                $.each(response, function (casa, modelli) {
                  $.each(modelli, function (nome, v) {
                    //console.log(nome);
                    let nomeAuto=nome;
                    options.push('<option value="' + nomeAuto + '">' + nomeAuto + '</option>');
                    //console.log(options);
                  });
                });
              
                // Inserisci tutte le opzioni in una volta
                $('#marca').html(options.join(''));
              }
        });
    }else{
        $.ajax({
            type: "get",
            url: "marchiAll",
            data: "data",
            success: function (response) {
                var options = []; // Array per le opzioni
                    options.push('<option value="" selected disabled>Seleziona Marca</option>')
                $.each(response, function (casa, modelli) {
                  $.each(modelli, function (nome, v) {
                    //console.log(nome);
                    let nomeAuto=nome;
                    options.push('<option value="' + nomeAuto + '">' + nomeAuto + '</option>');
                    //console.log(options);
                  });
                });
              
                // Inserisci tutte le opzioni in una volta
                $('#marca').html(options.join(''));
              }
        });
    }
});


$('#marca').change(function(e) {
    e.preventDefault();
    var marcaSelezionata = $(this).val();
    let reparto=$('#reparto').val();
    let modelloAuto;
    //console.log(marcaSelezionata);
    $.ajax({
        type: "post",
        url: "modelliAutoSel",
        data: {
            'marca':marcaSelezionata,
            'reparto':reparto
        },
        success: function (response) {
            console.log(response);
            var optionsModel = [];
            /* optionsModel.push('<option value="" selected disabled>--</option>') */
            //console.log(response);
            $.each(response, function (i, element) { 
                //console.log(element.modello);
                if (reparto==1) {
                    modelloAuto=element.modello  
                }else{
                
                    modelloAuto=i
                }
                optionsModel.push('<option value="' + modelloAuto + '">' + modelloAuto + '</option>');
                //console.log(optionsModel);
            });
            $('#modello').html(optionsModel.join(''));
        }
    });
});

$('#modello').click(function (e) { 
    e.preventDefault();
    var selectedValue = $('#modello').find("option").length;
    
    //console.log(selectedValue);
    
});

$('#nuovaAuto').click(function (e) { 
    e.preventDefault();
    $('#formNewAuto').removeClass('d-none')
    $('#formAllAuto').addClass('d-none')

});

$('#modificaAuto').click(function (e) { 
    e.preventDefault();
    $('#formNewAuto').addClass('d-none')
    $('#formAllAuto').removeClass('d-none')
});

/* $('#submitRicerca').click(function (e) { 
    e.preventDefault();
    let modello=$('#modello').val();
    let marca=$('#marca').val();

    console.log("modello "+modello+ " marca: "+marca);

    $.ajax({
        type: "post",
        url: "ricercaResult",
        data: {
            modello:modello,
            marca:marca
        },
        success: function (response) {
            console.log(response.length);
        }
    });
}); */