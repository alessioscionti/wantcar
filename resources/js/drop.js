import Dropzone from 'dropzone';
window.Dropzone = Dropzone;
if ($("#drophere").length>0) {
    let immagini=[];
    //console.log("ok");
    let csrfToken = $('meta[name="csrf-token"]').attr('content');
    let secret = $('#secret').val();
    let myDropzone = new Dropzone('#drophere', {
        url: '/imagesLoadCar',
        maxFilesize: 30, //in MB
        clickable:true,
        acceptedFiles: ".jpeg,.jpg,.png,.pdf", //accepted file types
        method: "POST",
        paramName: "file",
        addRemoveLinks: true ,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        params: {
            _token: csrfToken,
            secret: secret
        },
        init : function(){
            let dropzone = this
             
  
            this.on("sending", function(file,xhr, formData){
                formData.append("secret",secret)
                formData.append("immagini",immagini)
                
               
            });
            this.on("removedfile", function(file){
                console.log(immagini);
                
                $.ajax({
                  type: 'post',
                  url: '/imagesLoadCar/delete',
                  data: {
                   _token: csrfToken,
                   nome: immagini[file.name],
                   secret: secret,
                   immagini:immagini
                  },
                  success:function(response){
                    console.log(response);
                }
                });
              });
  
              this.on("success", function(file, response){
                //console.log(response);
                $.each(response, function (name) { 
                    //console.log(filename);
                    immagini[file.name]=response.filename;
                    immagini["path"]=response.pathName;
                    //immagini["path"]=pathname;
                    //immagini.push([""+file.name+"",nome])
                    
                    //file.name = name;
                    //console.log(immagini);
                });
                
                console.log(immagini);
             });
          }
    });
}