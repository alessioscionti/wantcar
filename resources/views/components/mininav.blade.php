<div class="container-fluid bg-dark mininav hidden-xs">
    <div class="row justify-content-center">
      <div class="col-1 d-flex text-center" style="margin: 0 auto;">
       <a href="{{route('h')}}"> <img class="img-fluid logo" width="100px" src="{{asset('storage/immagini/Opera_senza_titolo2.png')}}" alt="" ></a>
      </div>
      <div class="col-11 d-flex align-items-center" style="margin: 0 auto;">
        <h5>La tua prossima avventura inizia qui.</h5>
      </div>
    </div>
  </div>