<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @vite(['resources/js/app.js', 'resources/css/app.css'])
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css"
        integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Exo+2&family=Orbitron:wght@400..900&display=swap"
        rel="stylesheet">
    <title>Wantcar</title>
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
</head>

<body>
    <x-mininav />
    <x-navbar />
    {{ $slot }}
    <x-footer />

</body>
<script>
    $(document).ready(function() {
        var scrollTop = $(window).scrollTop();
        const screenWidth = window.innerWidth;
        const breakpoint = 768;
        $('.navbar').attr('style', 'background:none!important;border:none!important');
        $('.mininav').hide(400)
        $('.navbar').removeClass('bg-dark')
        
        if (screenWidth < breakpoint) {
            $('.logo').removeClass('d-none')
            if (scrollTop > 50) {
            /* $('#navbarNav').attr('style':'display:none') */
        }
        }
        $('.mininav').show(400)
        $('.navbar-toggler').attr('style','--bs-bg-opacity:0')

        
    });
    $(window).scroll(function(e) {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 1) {
            $('.navbar').removeClass('bg-dark')
            /* $('.logo').attr('src', "{{ asset('storage/immagini/Opera_senza_titolo1.png') }}"); */
            $('.mininav').hide(400)
            $('.hrefnav').removeClass('text-gold');
            $('.hrefnav').addClass('text-black');
        } else {
            $('.navbar').addClass('bg-dark')
            $('.logo').attr('src', "{{ asset('storage/immagini/Opera_senza_titolo2.png') }}");
            $('.mininav').show(400)
            $('.hrefnav').removeClass('text-black');
            $('.hrefnav').addClass('text-gold');
        }

        //console.log(scrollTop);
    });

    $('.navbar-toggler').click(function(e) {
        e.preventDefault();
        console.log(e);
        if ($('#navbarNav').hasClass('opened')) {
            $('#navbarNav').hide();
            $('#navbarNav').removeClass('opened')
            $('#navbarNav').addClass('closed')

        } else {
            $('#navbarNav').show();
            $('#navbarNav').addClass('opened')
        }
    });
</script>

</html>
