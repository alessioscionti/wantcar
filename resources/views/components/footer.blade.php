<style>
  a{
    text-decoration: none;
    color: white;
    cursor: pointer;
  }
</style>
<div class="bodyfooter">
    <footer>
      <div class="background">
       
      </div>
      <section>
        <ul class="socials">
          <li class="nav-item"><i class="fa-brands fa-square-facebook facebookBG"></i></li>
        <li class="nav-item"><i class="fa-brands fa-square-instagram instagramBG"></i></li>
        <li class="nav-item"><i class="fa-brands fa-tiktok tiktokBG"></i></li>
        </ul>
        <ul class="links">
          <li class="footerLinks"><a href="{{route('h')}}">Home</a></li>
          <li class="footerLinks"><a>Chi Siamo</a></li>
          <li class="footerLinks"><a>Auto</a></li>
          <li class="footerLinks"><a>Contatti</a></li>
          <li class="footerLinks"><a>Promozioni</a></li>
        </ul>
        <p class="legal">© 2024 All rights reserved</p>
      </section>
    </footer>
</div>