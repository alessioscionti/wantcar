{{-- <nav>
    <a href="#" style="color:goldenrod">Chi Siamo</a>
    <a href="#" style="color:goldenrod">Auto</a>
	<img class="img-fluid m-3" width="80px" src="{{asset('storage/immagini/want_car2_nobg.png')}}" alt="">
	<a href="#" style="color:goldenrod">Contatti</a>
	<a href="#" style="color:goldenrod">Support</a>
</nav> --}}
<nav class="navbar navbar-expand-lg navbar-light bg-dark">
  <div class="container-fluid">
    <div class="row justify-content-around">
      <div class="col-6 ms-0">
        <button class="navbar-toggler bg-dark mt-3 ms-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="collapseWidthExample" style="border: none">
          <i class="fa-solid fa-caret-down fs-1 text-white"></i>
        </button>
      </div>
      <div class="col-6 me-0">
        <a href="{{route('h')}}"><img class="img-fluid logo d-none" width="100px" src="{{asset('storage/immagini/Opera_senza_titolo2.png')}}" alt="" ></a>
      </div>
    </div>
    
    <div class="collapse collapse-horizontal navbar-collapse justify-content-center" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
            <a href="#" class="text-gold hrefnav">Chi Siamo</a>
        </li>
        <li class="nav-item">
            <a href="#" class="text-gold hrefnav">Auto</a>
        </li>
        <li class="nav-item">
            <a href="#" class="text-gold hrefnav">Contatti</a>
        </li>
        <li class="nav-item">
          <a href="#" class="text-gold hrefnav">Promozioni</a>
      </li>
      <li class="nav-item">
        <a href="#" class="text-gold hrefnav">Contatti</a>
    </li>
      </ul>
      <ul class="navbar-nav">
        <li class="nav-item"><i class="fa-brands fa-square-facebook facebookBG"></i></li>
        <li class="nav-item"><i class="fa-brands fa-square-instagram instagramBG"></i></li>
        <li class="nav-item"><i class="fa-brands fa-tiktok tiktokBG"></i></li>
      </ul>
    </div>
  </div>
</nav>

