<x-layout>
    <section class="masthead img-fluid">
        <div class="container-fluid pt-5">
            <div class="row pt-5">
                <div class="col-12">

                    {{-- <h1 class="pt-5">Want <span style="color:goldenrod">Car</span></h1> --}}
                </div>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="container mt-n5">
            <div class="row position-relative justify-content-center rowtrebox">
                <div
                    class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-3 mr-1 ml-1 mb-3 text-center trebox numero-cerchio">
                    <div class="counter">
                        <span class="marca" id="abarth">0</span>
                    </div>
                    <img class="img-fluid" src="{{ asset('storage/immagini/abarth.png') }}" alt="">
                </div>
                <div
                    class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-3 mr-1 ml-1 mb-3  text-center trebox numero-cerchio">
                    <div class="counter">
                        <span class="marca" id="bmw">0</span>
                    </div>
                    <img class="img-fluid" src="{{ asset('storage/immagini/bmw.png') }}" alt="">
                </div>
                <div
                    class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-3 mr-1 ml-1 mb-3  text-center trebox numero-cerchio">
                    <div class="counter">
                        <span class="marca" id="Alfa_romeo">0</span>
                    </div>
                    <img class="img-fluid" src="{{ asset('storage/immagini/alfa.png') }}" alt="">
                </div>
                <div
                    class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-3 mr-1 ml-1 mb-3  text-center trebox numero-cerchio">
                    <div class="counter">
                        <span class="marca" id="Fiat">0</span>
                    </div>
                    <img class="img-fluid" src="{{ asset('storage/immagini/fiat.png') }}" alt="">
                </div>
                <div
                    class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-3 mr-1 ml-1 mb-3  text-center trebox numero-cerchio">
                    <div class="counter">
                        <span class="marca" id="amg">0</span>
                    </div>
                    <img class="img-fluid" src="{{ asset('storage/immagini/amg.png') }}" alt="">
                </div>
                <div
                    class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-3 mr-1 ml-1 mb-3  text-center trebox numero-cerchio">
                    <div class="counter">
                        <span class="marca" id="audi">0</span>
                    </div>
                    <img class="img-fluid" src="{{ asset('storage/immagini/audi.png') }}" alt="">
                </div>
                <div
                    class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-3 mr-1 ml-1 mb-3  text-center trebox numero-cerchio">
                    <div class="counter">
                        <span class="marca" id="ferrari">0</span>
                    </div>
                    <img class="img-fluid" src="{{ asset('storage/immagini/ferrari.png') }}" alt="">
                </div>
                <div
                    class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-3 mr-1 ml-1 mb-3  text-center trebox numero-cerchio">
                    <div class="counter">
                        <span class="marca" id="ford">0</span>
                    </div>
                    <img class="img-fluid" src="{{ asset('storage/immagini/ford.png') }}" alt="">
                </div>
                <div
                    class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-3 mr-1 ml-1 mb-3  text-center trebox numero-cerchio">
                    <div class="counter">
                        <span class="marca" id="Mini">0</span>
                    </div>
                    <img class="img-fluid pt-3" src="{{ asset('storage/immagini/mini.png') }}" alt="">
                </div>
                <div
                    class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-3 mr-1 ml-1 mb-3  text-center trebox numero-cerchio">
                    <div class="counter">
                        <span class="marca" id="mini">0</span>
                    </div>
                    <img class="img-fluid" src="{{ asset('storage/immagini/jeep.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>
    <hr>
    <section class="section">
        <div class="container-fluid p-5">
            <div class="row justify-content-between">
                <div class="col-12 col-xs-12 col-md-3 col-lg-3 col-xl-3 text-center ricercaAuto mb-5 mt-5">
                    <h3 class="p-2">RICERCA AUTO</h3>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-12 w-75">
                                <form action="{{route('ricercaResult')}}" class=" p-5 ps-3 pe-3" method="post">
                                    @csrf
                                    <div class="form-group text-start mb-4">
                                        <label for="exampleInputEmail1">Marca</label><br>
                                        <select name="marca" class="form'control w-100 ricerca" id="marca">
                                            <option value="" disabled selected>--</option>
                                        </select>
                                    </div>

                                    <div class="form-group text-start mb-4">
                                        <label for="exampleInputEmail1">Modello</label><br>
                                        <select name="modello" class="form'control w-100 ricerca" id="modello">
                                            <option value="">Selezionare prima la marca</option>
                                        </select>
                                    </div>

                                    <div class="form-group text-start mb-4">
                                        <label for="exampleInputEmail1">Condizioni</label><br>
                                        <select name="condizioni" class="form'control w-100 ricerca" id="condizioni">
                                            <option value="nuovo">Nuovo</option>
                                            <option value="usato">Usato</option>
                                            <option value="km0">Km 0</option>
                                        </select>
                                    </div>

                                    <button type="submit" id="submitRicerca" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-xs-12 col-md-9 col-lg-9 col-xl-9 text-center d-none" id="ricerca">
                    @if (isset($ricerca))
                        
                    <input type="hidden" id="ricercaInput" value="{{$ricerca}}">
                    @else
                    <input type="hidden" id="ricercaInput" value="0">
                    
                    @endif
                    <h3>Risultati di ricerca</h3>
                    @foreach ($cerca as $garage)
                    <div class="vehicle-card mt-2">
                        <div class="details">
                            <div class="thumb-gallery">
                                @foreach ($garage->Image as $img)
                                    
                                <img class="first imgslide"
                                    src="{{asset($img->path)}}" />
                                @endforeach
                            </div>
                            <div class="info">
                                <h3 class="mt-1">{{$garage->marca}} - {{$garage->modello}}</h3>
                                <div class="price">
                                    <span>Prezzo</span>
                                    <h4 style="color: goldenrod">{{number_format($garage->prezzo,0,',','')}}€</h4>
                                </div>
                                <div class="ctas">
                                    <form action="{{route('detailCar',$garage->id)}}" method="post">
                                    @csrf
                                    <button type="submit" class="btn primary">Dettagli</button>
                                    </form>
                                    
                                    <div style="clear:both;"></div>
                                </div>
                                <div class="desc">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-12">
                                                <ul class="ulCard text-start">
                                                    <li>Alimentazione: <span style="color:goldenrod">{{$garage->alimentazione}}</span></li>
                                                    <li>Anno: <span style="color:goldenrod">{{$garage->anno}}</span></li>
                                                    <li>Km: <span style="color:goldenrod">{{$garage->km}}</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="specs">
                                    <div class="spec mpg">
                                        <span>Cv</span>
                                        <p>{{$garage->cavalli}}</p>
                                    </div>
                                    <div class="spec mpg">
                                        <span>Colore</span>
                                        <p>{{$garage->colore}}</p>
                                    </div>
                                    <div class="spec mpg">
                                        <span>Cilindrata</span>
                                        <p>{{$garage->cilindrata}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    @endforeach
                </div>
                <div class="col-12 col-xs-12 col-md-9 col-lg-9 col-xl-9 text-center" id="vetrina">
                    <h3>Auto in vetrina</h3>
                    @foreach ($garages as $garage)
                    <div class="vehicle-card mt-2">
                        <div class="details">
                            <div class="thumb-gallery">
                                @foreach ($garage->Image as $img)
                                    
                                <img class="first imgslide"
                                    src="{{asset($img->path)}}" />
                                @endforeach
                            </div>
                            <div class="info">
                                <h3 class="mt-1">{{$garage->marca}} - {{$garage->modello}}</h3>
                                <div class="price">
                                    <span>Prezzo</span>
                                    <h4 style="color: goldenrod">{{number_format($garage->prezzo,0,',','')}}€</h4>
                                </div>
                                <div class="ctas">
                                    <form action="{{route('detailCar',$garage->id)}}" method="post">
                                    @csrf
                                    <button type="submit" class="btn primary">Dettagli</button>
                                    </form>
                                    
                                    <div style="clear:both;"></div>
                                </div>
                                <div class="desc">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-12">
                                                <ul class="ulCard text-start">
                                                    <li>Alimentazione: <span style="color:goldenrod">{{$garage->alimentazione}}</span></li>
                                                    <li>Anno: <span style="color:goldenrod">{{$garage->anno}}</span></li>
                                                    <li>Km: <span style="color:goldenrod">{{$garage->km}}</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="specs">
                                    <div class="spec mpg">
                                        <span>Cv</span>
                                        <p>{{$garage->cavalli}}</p>
                                    </div>
                                    <div class="spec mpg">
                                        <span>Colore</span>
                                        <p>{{$garage->colore}}</p>
                                    </div>
                                    <div class="spec mpg">
                                        <span>Cilindrata</span>
                                        <p>{{$garage->cilindrata}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    @endforeach

                </div>
            </div>
        </div>
    </section>
    <hr>
    <section class="section mt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <h3>Ultimi arrivi</h3>
                </div>
            </div>
            <div class="row justify-content-center">
                @foreach ($garages as $garage)
                <div class="col-12 col-xs-12 col-sm-12 col-md-4 col-lg-3 col-xl-3 mt-3 text-center" id="novita">
                    <div class="vehicle-card mt-2">
                        <div class="details">
                            <div class="thumb-gallery">
                                @foreach ($garage->Image as $img)
                                    
                                <img class="first imgslide"
                                    src="{{asset($img->path)}}" />
                                @endforeach
                            </div>
                            <div class="info">
                                <h3 class="mt-1">{{$garage->marca}} - {{$garage->modello}}</h3>
                                <div class="price">
                                    <span>Prezzo</span>
                                    <h4 style="color: goldenrod">{{number_format($garage->prezzo,0,',','')}}€</h4>
                                </div>
                                <div class="ctas">
                                    <form action="{{route('detailCar',$garage->id)}}" method="post">
                                    @csrf
                                    <button type="submit" class="btn primary">Dettagli</button>
                                    </form>
                                    
                                    <div style="clear:both;"></div>
                                </div>
                                <div class="desc">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-12">
                                                <ul class="ulCard text-start">
                                                    <li>Alimentazione: <span style="color:goldenrod">{{$garage->alimentazione}}</span></li>
                                                    <li>Anno: <span style="color:goldenrod">{{$garage->anno}}</span></li>
                                                    <li>Km: <span style="color:goldenrod">{{$garage->km}}</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="specs">
                                    <div class="spec mpg">
                                        <span>Cv</span>
                                        <p>{{$garage->cavalli}}</p>
                                    </div>
                                    <div class="spec mpg">
                                        <span>Colore</span>
                                        <p>{{$garage->colore}}</p>
                                    </div>
                                    <div class="spec mpg">
                                        <span>Cilindrata</span>
                                        <p>{{$garage->cilindrata}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <hr>
    <section class="section mt-5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 col-xs-12 col-md-12 col-lg-6 col-xl-6 text-center mt-5">
                    <h2>Contatti</h2>
                    <form method="post" action="" name="contact-form">
                        @csrf
                        <div class="row justify-content-center mt-2">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="name" class="colorefirst">Nome</label>
                                    <input name="name" id="name" type="text"
                                        class="form-control contactbg" placeholder="Il tuo nome...">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="email" class="colorefirst">Email</label>
                                    <input name="email" id="email" type="email"
                                        class="form-control contactbg" placeholder="La tua email...">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mt-2">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="subject" class="colorefirst">Telefono</label>
                                    <input name="telefono" id="telefono" type="text"
                                        class="form-control contactbg" placeholder="numero di telefono...">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mt-2">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="comments" class="colorefirst">Messaggio</label>
                                    <textarea name="comments" id="comments" rows="7" class="form-control contactbg"
                                        placeholder="Scrivi il tuo messagio..."></textarea>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row justify-content-center mt-2 mb-5">
                            <div class="col-12 text-center">
                                <button type="submit" name="send" class="btn btn-danger w-100 h-100 p-3">Invia</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</x-layout>

<script>
    $(document).ready(function () {
       let ricerca= $('#ricercaInput').val()
       if (ricerca==1) {
            $('#ricerca').removeClass('d-none');
            $('#vetrina').addClass('d-none');
            $('html, body').animate({
      scrollTop: $("#ricerca").offset().top
    }, 1000);
       }else{
            $('#ricerca').addClass('d-none');
            $('#vetrina').removeClass('d-none');
    }
    let modelliCount;
    let marcaAuto;
    let totale
    $.ajax({
        type: "post",
        url: "getTotaleMarchi",
        data: "data",
        success: function (response) {
            //console.log(response);
            $.each(response, function (index, value) {
                console.log(value);
                if (value.marca=="Alfa romeo") {
                    value.marca=="Alfa_romeo";
                }
                
                $('#'+value.marca).html(value.totale)
                
            });
        }
    });
    //console.log(marcaAuto);
    });
</script>
