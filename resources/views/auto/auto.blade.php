<x-app-layout>
    <style>
        .ck.ck-editor__main>.ck-editor__editable {
            background: rgb(55 65 81) !important;
            border-radius: 0;
        }

        .ck-content {
            height: 300px;
        }

        .ck-content p {
            color: white
        }


        ul li {
            margin-left: 2rem !important;
            color: white;
            font-size: 1rem;
        }

        ol li {
            margin-left: 2rem !important;
            color: white;
            font-size: 1rem;
        }
    </style>
    <div class="container-fluid mt-5">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h2 class="text-white">Gestione Auto</h2>
            </div>
        </div>
    </div>

    <div class="container-fluid mt-5">
        <div class="row justify-content-around">
            <div class="col-6 text-end">

                <button id="nuovaAuto"
                    class="bg-indigo-500 shadow-lg shadow-indigo-500/50 text-white px-4 py-2 rounded focus:outline-none">Inserisci
                    Auto</button>
            </div>
            <div class="col-6 text-start">
                <button id="modificaAuto"
                    class="ml-4 bg-indigo-500 shadow-lg shadow-indigo-500/50 text-white px-4 py-2 rounded focus:outline-none">Modifica
                    Auto</button>

            </div>
        </div>
    </div>

    <div class="container-fluid mt-5 d-none" id="formNewAuto">
        <div class="row justify-content-center">
            <div class="col-12 text-center">

                <form action="{{ route('storeCar') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="gestionale" id="reparto" value="1">
                    <input type="hidden" name="secret" id="secret" value="{{$secret}}">
                    <div class="form-group border bg-dark rounded">
                        <div class="container-fluid">
                            <div class="row bg-danger m-3 rounded">
                                <label class="text-white mt-2" for="marca">Info Base</label>

                            </div>
                            <div class="row">
                                <div class="col-6 p-3">
                                    <label class="text-white d-flex text-start"
                                        for="exampleInputEmail1">Marca</label><br>
                                    <select name="marca" class="form-control w-100 ricerca" id="marca">
                                        <option value="" disabled selected>--</option>
                                    </select>
                                </div>
                                <div class="col-6 p-3">
                                    <label class="text-white d-flex text-start"
                                        for="exampleInputEmail1">Modello</label><br>
                                    <select name="modello" class="form-control w-100 ricerca" id="modello">
                                        <option value="">Selezionare prima la marca</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Anno</label>
                                    <input class="form-control rounded" name="anno" type="text"
                                        placeholder="anno">
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Km</label>
                                    <input class="form-control rounded" name="km" type="text" placeholder="Km">
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Prezzo</label>
                                    <input class="form-control rounded" name="prezzo" type="text"
                                        placeholder="prezzo">
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Colore</label>
                                    <input class="form-control rounded" name="colore" type="text"
                                        placeholder="colore">
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Cilindrata</label>
                                    <input class="form-control rounded" name="cilindrata" type="text"
                                        placeholder="Cilindrata">
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Alimentazione</label>
                                    <select name="alimentazione" class="form-control w-100 ricerca" id="Alimentazione">
                                        <option value="Diesel">Diesel</option>
                                        <option value="Benzina">Benzina</option>
                                        <option value="Elettrica">Elettrica</option>
                                        <option value="Ibrida">Ibrida</option>
                                    </select>
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Cambio</label>
                                    <select name="cambio" class="form-control w-100 ricerca" id="cambio">
                                        <option value="Manuale">Manuale</option>
                                        <option value="Automatico">Automatico</option>
                                    </select>
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Cavalli</label>
                                    <input class="form-control rounded" name="cavalli" type="text"
                                        placeholder="Cavalli">
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Auto in vetrina?</label>
                                    <select class="form-control w-100 ricerca" name="vetrina" id="vetrina">
                                        <option value="0" selected>NO</option>
                                        <option value="1">SI</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row bg-danger m-3 rounded">
                                <label class="text-white mt-2" for="marca">Info Avanzate</label>
                            </div>
                            <div class="row">
                                <label class="text-white d-flex text-start" for="anno">Immagini</label>
                                <div class="col-12 mt-5 mb-5 h-75">

                                    <div class="dropzone" id="drophere">
                                        
                                    </div>
                                </div>
                                <label class="text-white d-flex text-start" for="anno">Descrizione</label>
                                <div class="col-12 mt-5 mb-5 h-75">

                                    <textarea class="m-3" id="editor" name="info_aggiuntive"></textarea>
                                </div>
                            </div>
                            <div class="row justify-content-center mb-5">
                                <div class="col-12 text-center">
                                    <button id="salvaVeicolo"
                                        class="bg-indigo-500 shadow-lg shadow-indigo-500/50 text-white px-4 py-2 rounded focus:outline-none">Salva
                                        Auto</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid mt-5 d-none" id="formAllAuto">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                @foreach ($garages as $garage)
                <div class="vehicle-card mt-2">
                    <div class="details">
                        <div class="thumb-gallery">
                            @foreach ($garage->Image as $img)
                                
                            <img class="first imgslide"
                                src="{{asset($img->path)}}" />
                            @endforeach
                        </div>
                        <div class="info">
                            <h3 class="mt-1 text-white"><nobr>{{$garage->marca}} - {{$garage->modello}}</h3>
                            <div class="price text-start">
                                <span>Prezzo</span>
                                <h4 style="color: goldenrod">{{$garage->prezzo}}€</h4>
                            </div>
                            <div class="ctas">
                                <form action="{{route('editCar',$garage->id)}}" method="post">
                                @csrf
                                <button type="submit" class="btn primary">Modifica</button>
                                </form>
                                
                                <div style="clear:both;"></div>
                            </div>
                            <div class="desc">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-12 text-start">
                                            <ul class="ulCard text-start">
                                                <li>Alimentazione: <span style="color:goldenrod">{{$garage->alimentazione}}</span></li>
                                                <li>Anno: <span style="color:goldenrod">{{$garage->anno}}</span></li>
                                                <li>Km: <span style="color:goldenrod">{{$garage->km}}</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="specs">
                                <div class="spec mpg">
                                    <span>Cv</span>
                                    <p>{{$garage->cavalli}}</p>
                                </div>
                                <div class="spec mpg">
                                    <span>Colore</span>
                                    <p>{{$garage->colore}}</p>
                                </div>
                                <div class="spec mpg">
                                    <span>Cilindrata</span>
                                    <p>{{$garage->cilindrata}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    @endforeach
            </div>
        </div>

    </div>
    {{-- <script src="{{ asset('ckeditor/ckeditor.js') }}"></script> --}}
    {{-- <script src="https://cdn.ckeditor.com/ckeditor5/12.3.1/classic/ckeditor.js"></script> --}}
    <script src="assets/vendor/ckeditor5/build/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>

    
</x-app-layout>
