<x-app-layout>
    <style>
        .ck.ck-editor__main>.ck-editor__editable {
            background: rgb(55 65 81) !important;
            border-radius: 0;
        }

        .ck-content {
            height: 300px;
        }

        .ck-content p {
            color: white
        }


        ul li {
            margin-left: 2rem !important;
            color: white;
            font-size: 1rem;
        }

        ol li {
            margin-left: 2rem !important;
            color: white;
            font-size: 1rem;
        }
    </style>
    <div class="container-fluid mt-5">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h2 class="text-white">Gestione Auto</h2>
            </div>
        </div>
    </div>


    <div class="container-fluid mt-5">
        <div class="row justify-content-center">
            <div class="col-12 text-center">

                <form action="{{ route('modifiedCar', $garage) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="gestionale" id="reparto" value="1">
                    <input type="hidden" name="secret" id="secret" value="{{ $secret }}">
                    <div class="form-group border bg-dark rounded">
                        <div class="container-fluid">
                            <div class="row bg-danger m-3 rounded">
                                <label class="text-white mt-2" for="marca">Info Base</label>

                            </div>
                            <div class="row">
                                <div class="col-6 p-3">
                                    <label class="text-white d-flex text-start"
                                        for="exampleInputEmail1">Marca</label><br>
                                    <select name="marca" class="form-control w-100 ricerca" id="marcaEdit">
                                        <option value="{{ $garage->marca }}">{{ $garage->marca }}</option>
                                    </select>
                                </div>
                                <div class="col-6 p-3">
                                    <label class="text-white d-flex text-start"
                                        for="exampleInputEmail1">Modello</label><br>
                                    <select name="modello" class="form-control w-100 ricerca" id="modello">
                                        <option value="{{ $garage->modello }}">{{ $garage->modello }}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Anno</label>
                                    <input class="form-control rounded" name="anno" type="text" placeholder="anno"
                                        value="{{ $garage->anno }}">
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Km</label>
                                    <input class="form-control rounded" name="km" type="text" placeholder="Km"
                                        value="{{ $garage->km }}">
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Prezzo</label>
                                    <input class="form-control rounded" name="prezzo" type="text"
                                        placeholder="prezzo" value="{{ $garage->prezzo }}">
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Colore</label>
                                    <input class="form-control rounded" name="colore" type="text"
                                        placeholder="colore" value="{{ $garage->colore }}">
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Cilindrata</label>
                                    <input class="form-control rounded" name="cilindrata" type="text"
                                        placeholder="Cilindrata" value="{{ $garage->cilindrata }}">
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Alimentazione</label>
                                    <select name="alimentazione" class="form-control w-100 ricerca" id="Alimentazione">
                                        <option value="{{ $garage->alimentazione }}" selected>
                                            {{ $garage->alimentazione }}</option>
                                        <option value="Diesel">Diesel</option>
                                        <option value="Benzina">Benzina</option>
                                        <option value="Elettrica">Elettrica</option>
                                        <option value="Ibrida">Ibrida</option>
                                    </select>
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Cambio</label>
                                    <select name="cambio" class="form-control w-100 ricerca" id="cambio">
                                        <option value="{{ $garage->cambio }}" selected>{{ $garage->cambio }}</option>
                                        <option value="Manuale">Manuale</option>
                                        <option value="Automatico">Automatico</option>
                                    </select>
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Cavalli</label>
                                    <input class="form-control rounded" name="cavalli" type="text"
                                        placeholder="Cavalli" value="{{ $garage->cavalli }}">
                                </div>
                                <div class="col-3 p-3">
                                    <label class="text-white d-flex text-start" for="anno">Auto in
                                        vetrina?</label>
                                    <select class="form-control w-100 ricerca" name="vetrina" id="vetrina">
                                        <option value="{{ $garage->vetrina }}" selected>
                                            @if ($garage->vetrina == 0)
                                                NO
                                            @else
                                                SI
                                            @endif
                                        </option>
                                        <option value="0" selected>NO</option>
                                        <option value="1">SI</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row bg-danger m-3 rounded">
                                <label class="text-white mt-2" for="marca">Info Avanzate</label>
                            </div>

                            <div class="row">
                                <label class="text-white d-flex text-start" for="anno">Immagini Caricate</label>
                                <div class="col-12 mt-5 mb-5 h-75">
                                    {{-- <div class="owl-carousel owl-theme owl-loaded"> --}}
                                    <div class="container-fluid">
                                        <div class="row">

                                            @foreach ($garage->Image as $img)
                                                <div class="col-3 me-0 mt-5" id="div{{ $img->id }}">
                                                    <img width="350px" class="img-fluid rounded"
                                                        src="{{ asset($img->path) }}" alt="">
                                                    <input type="hidden" id="img{{ $img->id }}"
                                                        value="{{ $img->id }}">
                                                    <button id="deleteIMG{{ $img->id }}" type="button"
                                                        class="btn btn-danger deleteIMG"> Elimina </button>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <label class="text-white d-flex text-start" for="anno">Immagini</label>
                                <div class="col-12 mt-5 mb-5 h-75">

                                    <div class="dropzone" id="drophere">

                                    </div>
                                </div>
                                <label class="text-white d-flex text-start" for="anno">Descrizione</label>
                                <div class="col-12 mt-5 mb-5 h-75">

                                    <textarea class="m-3" id="editor" name="info_aggiuntive">{{ $garage->info_avanzate }}</textarea>
                                </div>
                            </div>
                            <div class="row justify-content-center mb-5">
                                <div class="col-12 text-center">
                                    <button id="salvaVeicolo"
                                        class="bg-indigo-500 shadow-lg shadow-indigo-500/50 text-white px-4 py-2 rounded focus:outline-none">Salva
                                        Auto</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="assets/vendor/ckeditor5/build/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>

    <script>
        $('.deleteIMG').click(function(e) {
            e.preventDefault();
            let idIMG = $(this).parent().find('input').val();
            if (confirm("vuoi cancellare l'immagine?")) {

                $.ajax({
                    type: "post",
                    url: "deleteIMG",
                    data: {
                        id: idIMG
                    },

                    success: function(response) {
                        
                        $('#div' + response).remove();
                    }
                });
            }
        });
    </script>

</x-app-layout>
