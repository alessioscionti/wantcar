<x-layout>
    <style>
        li.nav-item::before{
        content:"";
        
        }
        li.footerLinks::before{
        content:"";
        
        }
        li::before {
            content: "✓";

            width: 1em;
            margin-right: .5em;
            color: goldenrod
        }

        ul {
            display: flex;
            /* Attiva il layout Flexbox */
            flex-wrap: wrap;
            /* Permette il wrapping delle colonne */
            max-columns: 3;
            /* Imposta il numero massimo di colonne (3 in questo caso) */
        }

        li {
            flex: 1 0 auto;
            /* Distribuisce lo spazio equamente tra gli elementi */
            margin: 10px;
            /* Aggiunge margine per separare gli elementi */
        }
    </style>
    <section class="section mt-5">

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 text-center mt-5">
                    <h2 class="text-white">{{ $garage->marca }} {{ $garage->modello }}</h2>
                </div>
            </div>
            <div class="row justify-content-center mt-5">
                <div class="col-12 col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 text-center">
                    <div class="owl-carousel owl-theme owl-loaded">
                        @foreach ($garage->Image as $img)
                            <picture>
                                <source class="owl-lazy" media="(max-width: 350px)" data-srcset="{{ asset($img->path) }}">
                                <img class="owl-lazy rounded" data-src="{{ asset($img->path) }}" alt="">
                            </picture>
                        @endforeach

                    </div>
                </div>
                <div class="col-12 col-xs-12 col-sm-12 col-md-7 col-lg-7 col-xl-7 text-center">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <h2>Scheda Veicolo</h2>
                            </div>
                        </div>
                        <div class="row justify-content-start">
                            <div class="col-5 text-start ml-0 mr-2">
                                <p class="fs-5">MARCA: <span style="color:goldenrod">{{ $garage->marca }}</span></p>
                            </div>
                            <div class="col-5 text-start">
                                <p class="fs-5">MODELLO: <span style="color:goldenrod">{{ $garage->modello }}</span>
                                </p>
                            </div>

                            <div class="col-5 text-start" style="margin-top:-4rem">
                                <p class="fs-5">ANNO: <span style="color:goldenrod">{{ $garage->anno }}</span></p>
                            </div>
                            <div class="col-5 text-start" style="margin-top:-4rem">
                                <p class="fs-5">KM: <span style="color:goldenrod">{{ $garage->km }}</span></p>
                            </div>
                            <div class="col-5 text-start" style="margin-top:-4rem">
                                <p class="fs-5">COLORE: <span style="color:goldenrod">{{ $garage->colore }}</span></p>
                            </div>
                            <div class="col-5 text-start" style="margin-top:-4rem">
                                <p class="fs-5">ALIMENTAZIONE: <span
                                        style="color:goldenrod">{{ $garage->alimentazione }}</span></p>
                            </div>
                            <div class="col-5 text-start" style="margin-top:-4rem">
                                <p class="fs-5">CILINDRATA: <span
                                        style="color:goldenrod">{{ $garage->cilindrata }}</span></p>
                            </div>
                            <div class="col-5 text-start" style="margin-top:-4rem">
                                <p class="fs-5">CAMBIO: <span style="color:goldenrod">{{ $garage->cambio }}</span>
                                </p>
                            </div>
                            <div class="col-5 text-start" style="margin-top:-4rem">
                                <p class="fs-5">CAVALLI: <span style="color:goldenrod">{{ $garage->cavalli }}</span>
                                </p>
                            </div>
                            <div class="col-5 text-start" style="margin-top:-4rem">
                                <p class="fs-5">PREZZO: <span
                                        style="color:goldenrod">{{ number_format($garage->prezzo, 0, ',', '') }}
                                        €</span>
                                </p>
                            </div>
                        </div>
                        <div class="row justify-content-center my-n5">
                            @if (!$garage->info_avanzate)
                            @else
                            <hr>
                            <div class="col-12">
                                <h2>OPTIONAL</h2>
                                <div class="container-fluid">
                                    <div class="row justify-content-center">

                                        <div class="col-12 text-start">
                                            {!! $garage->info_avanzate !!}
                                        </div>
                                    </div>
                                </div>

                            </div>
                            @endif
                            <hr>
                            <div class="col-12">
                                <h2>RICHIEDI INFO</h2>
                                <form method="post" action="" name="contact-form">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="name" class="colorefirst">Nome</label>
                                                <input name="name" id="name" type="text"
                                                    class="form-control contactbg" placeholder="Il tuo nome..."
                                                    style="color:goldenrod;placeholder-color:white;">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="email" class="colorefirst">Email</label>
                                                <input name="email" id="email" type="email"
                                                    class="form-control contactbg" placeholder="La tua email..."
                                                    style="color:goldenrod">
                                            </div>
                                        </div>
                                    </div>
        
                                    <div class="row mt-2">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="subject" class="colorefirst">Telefono</label>
                                                <input name="telefono" id="telefono" type="text"
                                                    class="form-control contactbg" placeholder="numero di telefono..."
                                                    style="color:goldenrod">
                                            </div>
                                        </div>
                                    </div>
        
                                    <div class="row mt-2">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="comments" class="colorefirst">Messaggio</label>
                                                <textarea name="comments" id="comments" rows="7" class="form-control contactbg"
                                                    placeholder="Scrivi il tuo messagio..." style="color:goldenrod"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-12">
                                            <button name="send" class="submitBnt btn btn-custom contactbg">Invia
                                                Messaggio</button>
                                            <div id="simple-msg"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function() {
            $(".owl-carousel").owlCarousel();
        });
    </script>
</x-layout>
