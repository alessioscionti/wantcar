<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\Garage;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        
        $arrayGarage=array();
        $cars = Car::all()->groupBy('marca');
        //dd($cars);
        $count=Garage::get()->groupBy('marca');
        foreach ($count as $key => $value) {
            
            $arrayGarage=array(
                "marca"=>$key,
                "totale"=>$count->count(),
            );
        }
        //$count->count();
        //dd($arrayGarage);
        $garages=Garage::with("Image")->get();
        //dd($garage);
        foreach ($garages as $key) {
            foreach ($key->Image as $img) {
                //dd($img->path);
            }
            //dd($key->Image);
        }
        $cerca=Garage::all();
        $ricerca=0;
        return view('home',compact('cars','garages','cerca','ricerca'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
