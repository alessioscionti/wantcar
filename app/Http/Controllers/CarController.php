<?php

namespace App\Http\Controllers;

use session;
use App\Models\Car;
use App\Models\Garage;
use App\Models\Marchi;
use Illuminate\Http\Request;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $secret=base_convert(sha1(uniqid(mt_rand())),16,36);
        //dd($secret);
        $marchi=Marchi::all();
        $garages=Garage::all();
        $cerca=Garage::all();
        $ricerca=0;
        //dd(session()->all());
        //session()->forget("images");
        return view('auto.auto',compact('marchi','secret','garages','cerca','ricerca'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Car $car)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Car $car,$id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Car $car)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Car $car)
    {
        //
    }

    public function MarchiAll(Request $request){
        if ($request->reparto==1) {
            $cars = Car::get('marca')->groupBy('marca');
        }else {
            $cars = Garage::get('marca')->groupBy('marca');
        }
        
        return response(['marchi'=>$cars]);
        
    }

    public function modelliAutoSel(Request $req){
        
        $marcaSel=$req->marca;
        if ($req->reparto==1) {
            # code...
            $findCar=Car::where('marca',$marcaSel)->get();
        }else {
            # code...
            $findCar=Garage::where('marca',$marcaSel)->get()->groupBy('modello');
        }
        
        return response()->json($findCar);
    }

    public function ricercaResult(Request $request){
        //dd($request->all());
        $garages=Garage::all();
        $cerca=Garage::where('marca',$request->marca)->where('modello',$request->modello)->get();
        if (!$request->marca) {
            $ricerca=0;
        }else {
            $ricerca=1;
        }
        return view('home',compact('cerca','ricerca','garages'));
    }

    public function getTotaleMarchi(){
        
        $totaleMarchi=Garage::all()->groupBy('marca');
        //$totaleMarchi->count();
        foreach ($totaleMarchi as $key => $value) {
            $array[]=array(
                "marca"=>$key,
                "totale"=>count($value),
            );
            
        }

        return response()->json($array);
    
    }
}
