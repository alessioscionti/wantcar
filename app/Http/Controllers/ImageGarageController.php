<?php

namespace App\Http\Controllers;

use App\Models\imageGarage;
use Illuminate\Http\Request;

class ImageGarageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(imageGarage $imageGarage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(imageGarage $imageGarage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, imageGarage $imageGarage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(imageGarage $imageGarage)
    {
        //
    }
}
