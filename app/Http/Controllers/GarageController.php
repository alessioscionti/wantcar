<?php

namespace App\Http\Controllers;

use App\Models\Garage;
use App\Models\Marchi;
use App\Models\imageGarage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GarageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //dd($request->all());

        $garage = Garage::create([
            "marca" => $request->marca,
            "modello" => $request->modello,
            "anno" => $request->anno,
            "km" => $request->km,
            "prezzo" => $request->prezzo,
            "colore" => $request->colore,
            "cilindrata" => $request->cilindrata,
            "alimentazione" => $request->alimentazione,
            "cambio" => $request->cambio,
            "cavalli" => $request->cavalli,
            "info_avanzate" => $request->info_aggiuntive,

        ]);
        $id = $garage->id;
        //dd($id);
        $storeImage = session()->get("images.{$request->secret}");
        foreach ($storeImage as $img) {
            Storage::move("public/temp/{$request->secret}", "public/immagini/" . $id . "/");
            $storeImageCar = imageGarage::create([
                "garage_id" => $id,
                "path" => "storage/immagini/" . $id . "/".$img.""
            ]);
        }

        return redirect()->back()->with('message','auto inserita');
        //dd($storeImageCar);
    }

    /**
     * Display the specified resource.
     */
    public function show(Garage $garage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Garage $garage,$id)
    {
        $garage=Garage::find($id);
        $secret=base_convert(sha1(uniqid(mt_rand())),16,36);
        //dd($garage);
        return view('auto.edit',compact('garage','secret'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Garage $garage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Garage $garage)
    {
        //
    }

    public function imagesLoadCar(Request $request)
    {
        $secret = $request->input('secret');
        $filename = $request->file('file')->store("public/temp/{$secret}");

        $name = explode("/", $filename);
        $realName = $name[3];

        $pathname = $name[2];
        session()->push("images.{$secret}", $realName);
        return response()->json(["pathName" => $pathname, "filename" => $realName]);
    }
    public function deleteImage(Request $request)
    {
        $filename = $request->id;
        $immagine = $request->nome;
        Storage::delete("public/temp/{$request->secret}/" . $immagine);
        $contafile = count(Storage::allFiles("public/temp/{$request->secret}/"));
        if ($contafile == 0) {
            Storage::deleteDirectory("public/temp/{$request->secret}/");
            session()->forget("images.{$request->secret}");
        }
        return response()->json(['messaggio' => "file rimossi"]);
    }

    public function detailCar(Garage $garage,$id){
        
        $garage=Garage::find($id);
        

        return view('auto.detail',compact('garage'));
    }

    public function modifiedCar(Garage $garage,Request $request) {
        //dd($request->secret);
        $garageUpdate=Garage::where('id',$garage->id)->update([
            "marca" => $request->marca,
            "modello" => $request->modello,
            "anno" => $request->anno,
            "km" => $request->km,
            "prezzo" => $request->prezzo,
            "colore" => $request->colore,
            "cilindrata" => $request->cilindrata,
            "alimentazione" => $request->alimentazione,
            "cambio" => $request->cambio,
            "cavalli" => $request->cavalli,
            "info_avanzate" => $request->info_aggiuntive,
        ]);
        $id = $garage->id;
        //dd(Storage::copy("public/temp/{$request->secret}", "public/immagini/" . $garage->id . "/"));
        $storeImage = session()->get("images.{$request->secret}");
        //dd($storeImage);
        foreach ($storeImage as $img) {
            Storage::copy("public/temp/{$request->secret}", "public/immagini/" . $id . "/");
            $storeImageCar = imageGarage::create([
                "garage_id" => $id,
                "path" => "storage/immagini/" . $id . "/".$img.""
            ]);
        }
        $contafile = count(Storage::allFiles("public/temp/{$request->secret}/"));
        if ($contafile == 0) {
            Storage::deleteDirectory("public/temp/{$request->secret}/");
            session()->forget("images.{$request->secret}");
        }
        $marchi=Marchi::all();
        $garages=Garage::all();
        $cerca=Garage::all();
        $ricerca=0;
        $secret=base_convert(sha1(uniqid(mt_rand())),16,36);
        return view('auto.auto',compact('marchi','secret','garages','cerca','ricerca'));
    }

    public function deleteIMG(Request $request){
        $id=$request->id;
        $immagine=imageGarage::find($request->id);
        $explode=explode('/',$immagine->path);
        
        Storage::delete("public/immagini/{$explode[2]}/{$explode[3]}");
        $immagine=imageGarage::find($request->id)->delete();
        return response()->json($id);
    }
}
