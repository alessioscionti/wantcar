<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    protected $fillable=[
      'marca',
      'modello',
      'anno_da',
      'anno_a' 
    ];

    public function Image(){
      return $this->belongsTo(imageGarage::class);
  }
}
