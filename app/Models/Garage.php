<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Garage extends Model
{
    use HasFactory;

    protected $fillable=[
        'marca',
        'modello',
        'anno',
        'km',
        'prezzo',
        'colore',
        'cilindrata',
        'alimentazione',
        'cambio',
        'cavalli',
        'info_avanzate',
    ];

    public function Image(){
        return $this->hasMany(imageGarage::class);
    }
}
