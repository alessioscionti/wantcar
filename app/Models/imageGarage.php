<?php

namespace App\Models;

use App\Http\Controllers\ImageGarageController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class imageGarage extends Model
{
    use HasFactory;

    protected $fillable = [
        "garage_id",
        "path",
    ];

    public function Garage()
    {
        return $this->belongsTo(Garage::class);
    }
}
